from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from .models import User


class CostumerSignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
       model = User

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_costumer = True
        user.save()
        return user


class DriverSignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User

    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_driver = True
        if commit:
            user.save()
        return user