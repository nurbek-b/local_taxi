from django.shortcuts import redirect
from django.views.generic import CreateView
from driver.forms import CostumerSignUpForm
from driver.models import User
from django.contrib.auth import login


class CostumerSignUpView(CreateView):
    model = User
    form_class = CostumerSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'costumer'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('soctumer:order_list')