from django.shortcuts import redirect, render
from django.views.generic import TemplateView


class SignUpView(TemplateView):
    template_name = 'registration/signup.html'


def home(request):
    if request.user.is_authenticated:
        if request.user.is_driver:
            return redirect('driver:orders')
        else:
            return redirect('costumer:order_list')
    return render(request, 'home.html')




