from django.contrib.auth import login
from django.shortcuts import redirect
from django.views.generic import CreateView
from driver.forms import DriverSignUpForm
from driver.models import User


class DriverSignUpView(CreateView):
    model = User
    form_class = DriverSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'driver'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('driver:order_list')