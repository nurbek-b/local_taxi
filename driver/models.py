from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    is_customer = models.BooleanField(default=False)
    is_driver = models.BooleanField(default=False)


class City(models.Model):
    city_name = models.CharField(max_length=100)


class Driver(models.Model):
    driver_name = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    car_model = models.CharField(max_length=50, blank=True, null=True)
    car_image = models.ImageField(null=True, blank=True)
    car_plate = models.CharField(max_length=20, unique=True, blank=True, null=True)
    seat_amount = models.IntegerField(choices=list(zip(range(1, 10), range(1, 10))), unique=True)
    phone_number = models.IntegerField()


class Orders(models.Model):
    driver_name = models.OneToOneField(Driver, on_delete=models.CASCADE)
    from_city = models.ForeignKey(City, related_name='from_city', on_delete=models.CASCADE)
    to_city = models.ForeignKey(City, related_name='to_city', on_delete=models.CASCADE)
    price = models.PositiveIntegerField()
    order_date = models.DateField(auto_now_add=True)
    order_time = models.TimeField(blank=True, null=True)
    pickup_location = models.CharField(max_length=255)
    passengers_amount = models.PositiveIntegerField(choices=list(zip(range(1,20), range(1, 20))), unique=True)
    comment = models.TextField()


class Costumer(models.Model):
    name = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    orders = models.ManyToManyField(Orders, related_name='costumers_orders')
    phone_num = models.CharField(max_length=20)










