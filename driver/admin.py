from django.contrib import admin
from .models import *


admin.site.register(User)
admin.site.register(Orders)
admin.site.register(Driver)
admin.site.register(Costumer)
admin.site.register(City)